package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.IronPantsDemo;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title=IronPantsDemo.TITLE;
		config.height=IronPantsDemo.HEIGHT;
		config.width=IronPantsDemo.WIDTH;
		new LwjglApplication(new IronPantsDemo(), config);
	}
}
