package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygdx.game.States.GameStateManager;
import com.mygdx.game.States.MenuState;

public class IronPantsDemo extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;

	public static Integer WIDTH = 480;
	public static Integer HEIGHT = 900;
	public static String TITLE = "IronPantsDemo_Silviu";

	private Music music;

	private GameStateManager gameStateManager;

	ShapeRenderer shapeRenderer;
	
	@Override
	public void create () {
		gameStateManager = new GameStateManager();

		music= Gdx.audio.newMusic(Gdx.files.internal("music1.mp3"));

		music.setVolume(0.1f);

		music.setLooping(true);

		music.play();

		gameStateManager.add(new MenuState(gameStateManager));

		batch = new SpriteBatch();
		Gdx.gl.glClearColor(1, 0, 0, 1);


	}

	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();
		gameStateManager.update(Gdx.graphics.getDeltaTime());
		gameStateManager.renderTexture(batch);
		batch.end();


		gameStateManager.renderShape();
	}
	
	@Override
	public void dispose () {
		gameStateManager.clear();
		batch.dispose();
		music.dispose();
	}
}
