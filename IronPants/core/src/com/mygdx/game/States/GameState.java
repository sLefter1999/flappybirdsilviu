package com.mygdx.game.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.IronPantsDemo;

import java.util.ArrayList;

import Sprite.Bird;
import Sprite.Tube;

public class GameState extends State {

    private static final int TUBE_SPACING=125;
    private static final int TUBE_COUNT=4;
    private static final int GROUND_COUNT=2;

    private int score;

    Texture backgroundTexture;

    BitmapFont bitmapFont;

    Bird bird;
    ArrayList<Tube> tubes;

    Texture groundTexture;
    Vector3 posFirstGround;
    Vector3 posSecondGround;
    Vector2 posScore;

    protected GameState(GameStateManager gsm) {
        super(gsm);
        backgroundTexture = new Texture("bg.png");

        score=0;

        bird = new Bird(30,250);

        tubes = new ArrayList<Tube>();

        bitmapFont = new BitmapFont();
        bitmapFont.setColor(Color.GOLD);

        groundTexture = new Texture("ground.png");

        for(int i=1;i<=TUBE_COUNT;i++)
            tubes.add(new Tube(i*(TUBE_SPACING+ Tube.TUBE_WIDTH)));

        posFirstGround = new Vector3(camera.position.x-(camera.viewportWidth/2),-45,0);
        posSecondGround = new Vector3(posFirstGround.x+groundTexture.getWidth(),-45,0);

        camera.setToOrtho(false,IronPantsDemo.WIDTH/2,IronPantsDemo.HEIGHT/2);

        camera.position.x=bird.getBirdPosition().x;

        posScore = new Vector2(camera.position.x,camera.position.y);
    }

    @Override
    protected void handleInput() {
        if(Gdx.input.justTouched() && bird.getBirdPosition().y<=camera.position.y+camera.viewportHeight)
        {
            bird.jump();
        }
    }

    @Override
    protected void update(float dt) {

        handleInput();
        bird.updateGravity(dt);

        if(bird.getBirdPosition().y<=groundTexture.getHeight()+posFirstGround.y)
            gsm.set(new GameState(gsm));

        camera.position.x=bird.getBirdPosition().x+80;

        for(Tube tube:tubes) {
            if (tube.getPosBotTubeTexture().x + Tube.TUBE_WIDTH < camera.position.x - (camera.viewportWidth / 2))
                tube.reposition((int) (tube.getPosBotTubeTexture().x + ((Tube.TUBE_WIDTH + TUBE_SPACING) * TUBE_COUNT)));

            if (tube.collision(bird.getBirdRectangle()))
                gsm.set(new GameState(gsm));

            if(bird.getBirdPosition().x>tube.getTopTubePosition().x+Tube.TUBE_WIDTH)
                score=score+1;
        }

        if(posFirstGround.x+groundTexture.getWidth()<camera.position.x-(camera.viewportWidth/2))
            posFirstGround.x=posSecondGround.x+groundTexture.getWidth();

        if(posSecondGround.x+groundTexture.getWidth()<camera.position.x-(camera.viewportWidth/2))
            posSecondGround.x=posSecondGround.x+groundTexture.getWidth();


        camera.update();
    }

    @Override
    protected void renderTexture(SpriteBatch spriteBatch)
    {

        spriteBatch.setProjectionMatrix(camera.combined);
        spriteBatch.draw(backgroundTexture,camera.position.x-camera.viewportWidth/2,0, IronPantsDemo.WIDTH,IronPantsDemo.HEIGHT);
        spriteBatch.draw(bird.getBirdTexture(),bird.getBirdPosition().x,bird.getBirdPosition().y);

        for(Tube tube:tubes) {
            spriteBatch.draw(tube.getTopTubeTexture(),tube.getTopTubePosition().x,tube.getTopTubePosition().y);
            spriteBatch.draw(tube.getBottomTubeTexture(),tube.getPosBotTubeTexture().x,tube.getPosBotTubeTexture().y);
        }

        spriteBatch.draw(groundTexture,posFirstGround.x,posFirstGround.y);
        spriteBatch.draw(groundTexture,posSecondGround.x,posSecondGround.y);

        bitmapFont.draw(spriteBatch,String.valueOf(score),camera.position.x-100,camera.position.y+200);

    }

    @Override
    protected void renderShape() {

    }

    @Override
    protected void dispose() {

        bird.dispose();

        for(Tube tube:tubes)
            tube.dispose();

        backgroundTexture.dispose();

        groundTexture.dispose();
    }
}
