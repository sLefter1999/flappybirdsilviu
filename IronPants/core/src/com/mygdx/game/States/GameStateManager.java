package com.mygdx.game.States;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.Stack;

public class GameStateManager
{
    Stack<State> states;

    public GameStateManager()
    {
        states = new Stack<State>();
    }

    public void add(State state)
    {
        states.push(state);
    }

    public void pop()
    {
        states.pop().dispose();
    }

    public void set(State state)
    {
        states.pop();
        states.push(state);
    }

    public void clear()
    {
        while(!states.empty())
        {
            pop();
        }
    }

    public void update(float dt)
    {
        states.peek().update(dt);
    }

    public void renderTexture(SpriteBatch spriteBatch)
    {
        states.peek().renderTexture(spriteBatch);
    }

    public void renderShape()
    {
        states.peek().renderShape();
    }
}
