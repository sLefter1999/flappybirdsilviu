package com.mygdx.game.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mygdx.game.IronPantsDemo;

import java.util.Stack;

public class MenuState extends State {

    Texture backgroundTexture;
    Texture playButtonTexture;

    ShapeRenderer shapeRenderer;

    public MenuState(GameStateManager gsm) {
        super(gsm);

        shapeRenderer = new ShapeRenderer();
        backgroundTexture = new Texture("background3.jpg");
        playButtonTexture = new Texture("PlayButton.png");
    }

    @Override
    protected void handleInput() {
        if(Gdx.input.justTouched())
            gsm.set(new GameState(gsm));
    }

    @Override
    protected void update(float dt) {
        handleInput();
    }

    @Override
    protected void renderTexture(SpriteBatch spriteBatch) {

        spriteBatch.draw(backgroundTexture,0,0);
        spriteBatch.draw(playButtonTexture,IronPantsDemo.WIDTH/2-playButtonTexture.getWidth()/2,IronPantsDemo.HEIGHT/2-playButtonTexture.getHeight()/2);
    }

    @Override
    protected void renderShape() {
    }

    @Override
    protected void dispose() {
        backgroundTexture.dispose();

        playButtonTexture.dispose();

        System.out.println("Menu dispose");
    }

}
