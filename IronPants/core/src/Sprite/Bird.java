package Sprite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class Bird
{
    Vector3 birdPosition;
    Vector3 birdVelocity;
    Texture birdTexture;


    private Animation birdAnimation;

    Rectangle birdRectangle;

    private static float GRAVITY=-15;
    private static float MOVEMENT=100;

    public Bird(int x,int y)
    {
        birdVelocity=new Vector3(0,0,0);
        birdPosition = new Vector3(x,y,0);

        birdTexture = new Texture("birdanimation.png");

        birdAnimation=new Animation(birdTexture,3,0.5f);

        birdRectangle = new Rectangle(birdPosition.x,birdPosition.y,birdAnimation.getFrame().getRegionWidth(),birdAnimation.getFrame().getRegionHeight());
    }

    public void updateGravity(float dt)
    {

        birdAnimation.update(dt);

        if(birdPosition.y>0)
        birdVelocity.add(0,GRAVITY,0);

        birdVelocity.scl(dt);

        birdPosition.add(MOVEMENT*dt,birdVelocity.y,0);

        birdVelocity.scl(1/dt);

        if(birdPosition.y<0)
            birdPosition.y=0;

        birdRectangle.set(birdPosition.x,birdPosition.y,birdAnimation.getFrame().getRegionWidth(),birdAnimation.getFrame().getRegionHeight());
    }

    public Rectangle getBirdRectangle(){return this.birdRectangle;}

    public TextureRegion getBirdTexture()
    {
        return this.birdAnimation.getFrame();
    }

    public Vector3 getBirdPosition()
    {
        return this.birdPosition;
    }

    public void jump()
    {
        birdVelocity.y=250;
    }

    public void dispose()
    {
        birdTexture.dispose();
    }
}
