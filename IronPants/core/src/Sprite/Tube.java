package Sprite;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import java.util.Random;

public class Tube
{
    public static final int TUBE_WIDTH=52;

    private static final int FLUCTUATION = 130;
    private static final int TUBE_GAP=100;
    private static final int LOWEST_OPENING=120;

    Texture topTubeTexture;
    Texture bottomTubeTexture;

    Vector3 posTobTubeTexture;
    Vector3 posBotTubeTexture;

    Rectangle rectangleTopTube;
    Rectangle rectangleBotTube;

    Random rand;

    public Tube(int x)
    {
        rand = new Random();

        topTubeTexture = new Texture("toptube.png");
        bottomTubeTexture = new Texture("bottomtube.png");

        posTobTubeTexture = new Vector3(x,TUBE_GAP+LOWEST_OPENING+rand.nextInt(FLUCTUATION),0);
        posBotTubeTexture = new Vector3(x,posTobTubeTexture.y-topTubeTexture.getHeight()-TUBE_GAP,0);

        rectangleTopTube= new Rectangle(posTobTubeTexture.x,posTobTubeTexture.y,topTubeTexture.getWidth(),topTubeTexture.getHeight());
        rectangleBotTube= new Rectangle(posBotTubeTexture.x,posBotTubeTexture.y,bottomTubeTexture.getWidth(),bottomTubeTexture.getHeight());
    }

    public Texture getTopTubeTexture()
    {
        return topTubeTexture;
    }

    public Texture getBottomTubeTexture()
    {
        return bottomTubeTexture;
    }

    public Vector3 getPosBotTubeTexture()
    {
        return posBotTubeTexture;
    }

    public Vector3 getTopTubePosition()
    {
        return posTobTubeTexture;
    }

    public void reposition(int x)
    {
        posTobTubeTexture.set(x,TUBE_GAP+LOWEST_OPENING+rand.nextInt(FLUCTUATION),0);

        rectangleTopTube.setPosition(posTobTubeTexture.x,posTobTubeTexture.y);

        posBotTubeTexture.set(x,posTobTubeTexture.y-topTubeTexture.getHeight()-TUBE_GAP,0);

        rectangleBotTube.setPosition(posBotTubeTexture.x,posBotTubeTexture.y);
    }

    public boolean collision(Rectangle rectangle)
    {
        return rectangleTopTube.overlaps(rectangle) || rectangleBotTube.overlaps(rectangle);

    }

    public void dispose()
    {
        topTubeTexture.dispose();
        bottomTubeTexture.dispose();
    }
}
